import React, { useContext } from 'react';
import styled from 'styled-components';

import objectFinder from '../../utils/objectFinder';
import StoreContext from './../../context/StoreContext';

const CartContainer = ({setAppData, showCart, setShowCart}:any) => {
    const appData:any = useContext(StoreContext);
    const { cart, cartTotal }:any = useContext(StoreContext);

    const amountButtonHandler = (e, id) => {
        const isAdding:boolean = e.target.innerText === '+' ? true : false;
        const itemIndex:number = objectFinder(cart, 'id', id);
        const newCart:any = [...cart];

        if(isAdding) {
            if(newCart[itemIndex].amount === newCart[itemIndex].stock) {
                return;
            }
            newCart[itemIndex].amount += 1;
        } else if(!isAdding && newCart[itemIndex].amount - 1 === 0) {
            newCart.splice(itemIndex, 1);
        } else {
            newCart[itemIndex].amount -= 1;
        }

        let newCartTotal = 0;
        for(const item of newCart) {
            const itemTotal = item.price * item.amount;
            newCartTotal += itemTotal;
        }

        setAppData({...appData, cart: newCart, cartTotal: newCartTotal});
    };

    const checkoutHandler = () => {
        if(cartTotal === 0) {
            return;
        }

        const newProductList = appData.products;
        for(const item of cart) {
            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            const newStock = item.stock - item.amount;
            const dataToUpdate = JSON.stringify({"stock":newStock});

            const requestOptions = {
            method: 'PATCH',
            headers: myHeaders,
            body: dataToUpdate,
            };

            fetch(`http://localhost:3000/grocery/${item.id}`, requestOptions)
            .then(response => response.json())
            .then(result => {
                const productIndex = objectFinder(appData.products, 'id', result.id);
                if(productIndex !== -1) {
                    newProductList[productIndex].stock = result.stock;
                }
                setAppData({ ...appData, products: [...newProductList], cart: [], cartTotal: 0 });
            })
            .catch(error => console.log('error', error));
        }
    };

    return (
        <StyledCart showCart={showCart}>
            <StyledCheckoutButton onClick={checkoutHandler}>Checkout {cartTotal}€</StyledCheckoutButton>
            {
                cart.map( (item) => {
                    return(
                        <StyledCartList key={item.id}>
                            <StyledCartItemImage image={item.image_url} />
                            <StyledCartItemAmount>
                                <p>{item.productName}</p>
                                <p><button onClick={(e) => amountButtonHandler(e, item.id)}>-</button> {item.amount} <button onClick={(e) => amountButtonHandler(e, item.id)}>+</button></p>
                            </StyledCartItemAmount>
                            <StyledCartItemPrice>
                                {item.price * item.amount}€
                            </StyledCartItemPrice>
                        </StyledCartList>
                    )
                } )
            }
        </StyledCart>
    )
};

export default CartContainer;

// Styles
const StyledCart = styled.div`
    align-items: flex-start;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    margin: 2rem 0;
    padding: 1rem;
    width: 20%;

    @media (max-width: 768px) {
        display: none;
    }
`;

const StyledCheckoutButton = styled.div`
    background-color: #fafafa;
    border: 2px solid black;
    cursor: pointer;
    font-size: 1.8rem;
    font-weight: bold;
    text-transform: uppercase;
    padding: 1rem;
    text-align: center;

    &:hover{
        background-color: #c9c9c9;
        border: 2px solid black;
    }
`;

const StyledCartList = styled.div`
    align-items: center;
    display: flex;
    justify-content: center;
    margin: 1rem 0;
    width: 100%;
`;

const StyledCartItemImage = styled.div`
    background: #000 url('${props => props.image}') no-repeat center center;
    height: 80%;
    overflow: hidden;
    width: 25%;
`;

const StyledCartItemAmount = styled.div`
    width: 50%;
`;

const StyledCartItemPrice = styled.div`
    font-size: 1.2rem;
    font-weight: bold;
    width: 25%;
`;